## How to start
1. npm install
2. ng serve --o

## How to make CK Editor with upload image from scratch
### Build Custom CKEditor 5
1. Open https://ckeditor.com/ckeditor-5/online-builder/
2. Choose editor type (Classic for example)
3. Picked plugins (Make sure image and base64 upload updater is picked)
4. Choose toolbar items (default choice)
5. Choose language (English for example)
6. Start and download your custom CKEditor 5 Build

### Add Dependencies
1. npm install --save @ckeditor/ckeditor5-angular
2. npm install --save @ckeditor/ckeditor5-upload

### Integrated Custom CKEditor 5 with Angular Project
1. Extract Custom CKEditor 5 zip file and rename the folder to ckeditor5 and then place in src angular folder
2. Edit ckeditor.js in ckeditor5 folder > src
3. Setup Base64UploadAdapter
3.1 Import the adapter : <br />
import Base64UploadAdapter from '@ckeditor/ckeditor5-upload/src/adapters/base64uploadadapter'; <br />
3.2 Add to the last plugins : <br />
Editor.builtinPlugins = [ <br />
    ...., <br />
    Base64UploadAdapter <br />
]
4. Change directory (cd) to ckeditor5 folder and then npm install and npm run build
5. Add CKEditorModule in every component module that using ckeditor and FormsModule for two way binding
6. Edit tsconfig.json and then add "allowJs": true in compilerOptions
7. Import ckeditor5 in component : <br />
import * as ClassicEditor from '../ckeditor5/build/ckeditor';
7. Add global field in component class : <br />
editor : any = ClassicEditor; <br />
data : string = '';
8. Setup html : <br />
{{data | json}} <br />
<ckeditor [editor]="editor" [(ngModel)]="data"></ckeditor> 
9. Run the app : ng serve --o